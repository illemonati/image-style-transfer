from utils import get_input_optimizer, save_image, device
from tqdm import tqdm
from torchvision import utils as vutils
from model import make_model
import torch
import os


def transfer(style_image, content_image, steps, style_weight, content_weight, output_name=None, save_every=50):
    model, style_losses, content_losses = make_model(
        style_image, content_image)
    optimizer = get_input_optimizer(content_image)
    pbar = tqdm(total=steps)

    for step in range(steps):
        global style_score, content_score, loss
        pbar.set_description(f'[{step+1}/{steps}]')

        def closure():
            global style_score, content_score, loss
            style_score = 0
            content_score = 0
            content_image.data.clamp_(0, 1)
            optimizer.zero_grad()
            model(content_image)

            for loss in style_losses:
                style_score += loss.loss

            for loss in content_losses:
                content_score += loss.loss

            style_score *= style_weight
            content_score *= content_weight

            loss = style_score + content_score
            loss.backward()

            return loss

        optimizer.step(closure)

        pbar.set_postfix({
            'style_score': style_score.item(),
            'content_score': content_score.item(),
            'loss': loss.item()
        })
        pbar.update(1)

        if output_name and (step % save_every == 0 or step == steps-1):
            save_image(content_image, output_name, step)
    content_image.data.clamp_(0, 1)
    return content_image
