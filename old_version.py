import errno

import numpy as np
import tensorflow as tf
from tensorflow.keras.applications import vgg19
import matplotlib.pyplot as plt
import PIL
from PIL import Image
import os

style_image_path = 'assets/spaghetti.jpg'
input_image_path = 'inputs/lion.jpg'
output_name = 'outputs/lion-spaghetti/lion'
mode = 'Adam'
keep_input_color = False
keep_style_color = True
total_variation_weight = 1e-6
style_weight = 1e-6
content_weight = 2.5e-8
output_height = 400
input_image = tf.keras.preprocessing.image.load_img(input_image_path)
output_width = int(input_image.width / input_image.height * output_height)
resize_and_crop = True


def preprocess_image(path, gray_scale=False):
    img: Image = tf.keras.preprocessing.image.load_img(
        path,
        target_size=None if resize_and_crop else (output_width, output_height)
    )
    if resize_and_crop:
        hratio = output_height/img.size[1]
        width = int(img.size[0]*hratio)
        img = img.resize((width, output_height))
        img = img.crop((0, 0, output_width, output_height))
    if gray_scale:
        img = img.convert('LA')
        img = img.convert('RGB')
    plt.imshow(img)
    plt.show()
    img = tf.keras.preprocessing.image.img_to_array(img)
    img = np.expand_dims(img, 0)
    img = vgg19.preprocess_input(img)
    return tf.convert_to_tensor(img)


def tensor_to_image(x):
    # Util function to convert a tensor into a valid image
    x = x.reshape((output_height, output_width, 3))
    # Remove zero-center by mean pixel
    x[:, :, 0] += 103.939
    x[:, :, 1] += 116.779
    x[:, :, 2] += 123.68
    # 'BGR'->'RGB'
    x = x[:, :, ::-1]
    x = np.clip(x, 0, 255).astype("uint8")
    return x


def gram_matrix(x):
    x = tf.transpose(x, (2, 0, 1))
    features = tf.reshape(x, (tf.shape(x)[0], -1))
    gram = tf.matmul(features, tf.transpose(features))
    return gram


def style_loss(style, combination):
    S = gram_matrix(style)
    C = gram_matrix(combination)
    channels = 3
    size = output_height * output_width
    return tf.reduce_sum(tf.square(S - C)) / (4.0 * (channels ** 2) * (size ** 2))


def content_loss(base, combination):
    return tf.reduce_sum(tf.square(combination - base))


def total_variation_loss(x):
    a = tf.square(x[:, : output_height - 1, : output_width -
                    1, :] - x[:, 1:, : output_width - 1, :])
    b = tf.square(x[:, : output_height - 1, : output_width -
                    1, :] - x[:, : output_height - 1, 1:, :])
    return tf.reduce_sum(tf.pow(a + b, 1.25))


model = vgg19.VGG19(weights='imagenet', include_top=False)
model.summary()

style_layers = [f'block{i}_conv1' for i in range(1, 6)]
outputs_dict = dict([(layer.name, layer.output) for layer in model.layers])
feature_extractor = tf.keras.Model(inputs=model.inputs, outputs=outputs_dict)
feature_extractor.summary()

content_layer_name = "block5_conv2"


@tf.function
def compute_loss(combination_image, base_image, style_reference_image):
    input_tensor = tf.concat(
        [base_image, style_reference_image, combination_image], axis=0
    )
    features = feature_extractor(input_tensor)

    # Initialize the loss
    loss = tf.zeros(shape=())

    # Add content loss
    layer_features = features[content_layer_name]
    base_image_features = layer_features[0, :, :, :]
    combination_features = layer_features[2, :, :, :]
    loss = loss + content_weight * content_loss(
        base_image_features, combination_features
    )
    # Add style loss
    for layer_name in style_layers:
        layer_features = features[layer_name]
        style_reference_features = layer_features[1, :, :, :]
        combination_features = layer_features[2, :, :, :]
        sl = style_loss(style_reference_features, combination_features)
        loss += (style_weight / len(style_layers)) * sl

    # Add total variation loss
    loss += total_variation_weight * total_variation_loss(combination_image)
    return loss


@tf.function
def compute_loss_and_grads(combination_image, base_image, style_reference_image):
    with tf.GradientTape() as tape:
        loss = compute_loss(combination_image, base_image,
                            style_reference_image)
    grads = tape.gradient(loss, combination_image)
    return loss, grads


optimizer = tf.keras.optimizers.SGD(
    tf.keras.optimizers.schedules.ExponentialDecay(
        initial_learning_rate=100.0, decay_steps=100, decay_rate=0.96
    )
)

if mode == 'Adam':
    optimizer = tf.keras.optimizers.Adam(lr=1)

input_image = preprocess_image(input_image_path, not keep_input_color)
style_image = preprocess_image(style_image_path, not keep_style_color)
combination_image = tf.Variable(preprocess_image(
    input_image_path, not keep_input_color))

iterations = 4000
for i in range(1, iterations + 1):
    loss, grads = compute_loss_and_grads(
        combination_image, input_image, style_image
    )
    optimizer.apply_gradients([(grads, combination_image)])
    if i % 100 == 0:
        print("Iteration %d: loss=%.2f lr:%.2f" %
              (i, loss, optimizer._decayed_lr(tf.float32)))
        img = tensor_to_image(combination_image.numpy())
        fname = output_name + "_iteration_%d.png" % i
        if not os.path.exists(os.path.dirname(fname)):
            try:
                os.makedirs(os.path.dirname(fname))
            except OSError as e:  # Guard against race condition
                if e.errno != errno.EEXIST:
                    pass
        tf.keras.preprocessing.image.save_img(fname, img)
