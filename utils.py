from PIL import Image
import torch
from torchvision import transforms, utils as vutils
import torch.optim as optim
import os


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

cnn_normalization_mean = torch.tensor([0.485, 0.456, 0.406]).to(device)
cnn_normalization_std = torch.tensor([0.229, 0.224, 0.225]).to(device)


def save_image(image, filename_base, steps):

    filename = f'{filename_base}-{steps}.png'

    if not os.path.exists(os.path.dirname(filename)):
        try:
            os.makedirs(os.path.dirname(filename))
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    file = open(filename, 'wb')
    vutils.save_image(image, file)
    file.close()


def get_normalizer():
    return transforms.Normalize(cnn_normalization_mean, cnn_normalization_std)


def get_input_optimizer(input_image):
    optimizer = optim.LBFGS([input_image.requires_grad_()])
    return optimizer


def preprocess_image(path, output_height, output_width, gray_scale=False):
    image = Image.open(path)
    height = output_height
    width = output_width
    if image.height > image.width:
        width = int(image.width * output_height / image.height)
    elif image.width > image.height:
        height = int(image.height * output_width / image.width)

    trans = transforms.Compose([
        transforms.ToTensor(),
        transforms.Resize((height, width)),
        transforms.CenterCrop((output_height, output_width))
    ])
    if gray_scale:
        trans = transforms.Compose([
            trans,
            transforms.Grayscale(3)
        ])
    processed_image = trans(image)
    return processed_image


def gram_matrix(x):
    batch_size, channels, height, width = x.shape
    features = x.view(batch_size * channels, height * width)
    gram_product = torch.mm(features, features.T)
    return gram_product.div(batch_size * channels * height * width)
