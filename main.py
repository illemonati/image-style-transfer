import numpy as np
import torch
import torch.nn as nn
import torchvision
from torchvision import transforms, datasets, utils as vutils
import matplotlib.pyplot as plt
import PIL
from PIL import Image
import os
import torch.nn.functional as F
from utils import preprocess_image, device
from model import make_model
from torch import optim
from torchinfo import summary
from transfer import transfer

style_image_path = 'assets/spaghetti.jpg'
input_image_path = 'inputs/lion.jpg'
output_name = 'outputs/lion-spaghetti/lion'

keep_input_color = False
keep_style_color = True
style_weight = 10000000
content_weight = 1
steps = 50
save_every = 10


input_image = Image.open(input_image_path)
output_height = 400
output_width = int(input_image.width / input_image.height * output_height)


input_image = preprocess_image(
    input_image_path, output_height, output_width, not keep_input_color).to(device)
style_image = preprocess_image(
    style_image_path, output_height, output_width, not keep_style_color).to(device)

print(input_image.shape)
print(style_image.shape)

assert input_image.shape == style_image.shape

plt.imshow(
    np.transpose(
        vutils.make_grid(
            [input_image, style_image], nrow=2
        ).cpu(),
        (1, 2, 0)
    )
)
# plt.show()

input_image = input_image.unsqueeze(0)
style_image = style_image.unsqueeze(0)

output_image = transfer(style_image, input_image, steps,
                        style_weight, content_weight, output_name, save_every)
