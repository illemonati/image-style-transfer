import torch.nn as nn
import torch.nn.functional as F
from utils import gram_matrix


class StyleLoss(nn.Module):
    def __init__(self, target_feature):
        super(StyleLoss, self).__init__()
        self.target = gram_matrix(target_feature).detach()

    def forward(self, x):
        x_gram_matrix = gram_matrix(x)
        self.loss = F.mse_loss(x_gram_matrix, self.target)
        return x
